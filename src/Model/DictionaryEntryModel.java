package Model;

import java.util.Date;

public class DictionaryEntryModel {

    private final String key;
    private final String value;

    private final Date createdAt;

    public DictionaryEntryModel(String key, String value) {
        this.key = key;
        this.value = value;
        this.createdAt = new Date();
    }

    public String getKey() {
        return this.key;
    }

    public String getValue() {
        return this.value;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

}
