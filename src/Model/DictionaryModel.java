package Model;

import java.util.ArrayList;
import java.util.List;

public class DictionaryModel {

    private final String name;
    private final List<DictionaryEntryModel> entries = new ArrayList<>();

    public DictionaryModel(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public List<DictionaryEntryModel> getEntries() {
        return this.entries;
    }

}
