package View;

import Controller.DictionaryController;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class DictionaryView extends JFrame {

    private final JLabel dictionaryLabel = new JLabel();
    private final JTable entryTable = new JTable(new DefaultTableModel(new String[]{"key", "value"}, 0));

    private final JComboBox<String> dictionaryDropdown = new JComboBox<>();
    private final JButton dictionaryDropdownButton = new JButton("Choose");

    private final JTextField keyTextField = new JTextField();
    private final JTextField valueTextField = new JTextField();

    private final JButton entryAddButton = new JButton("Add");

    public DictionaryView() {
        super("Dictionary GUI");
        this.loadGui();
    }

    private void loadGui() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 500);
        this.setLayout(null);
        this.setResizable(false);
        this.setLocation(430, 100);

        JPanel dictionaryPanel = new JPanel();
        dictionaryPanel.setBounds(0, 0, 500, 70);
        this.add(dictionaryPanel);

        JPanel tablePanel = new JPanel();
        tablePanel.setBounds(0, 70, 500, 200);
        this.add(tablePanel);

        JPanel entryPanel = new JPanel();
        entryPanel.setBounds(0, 350, 500, 50);
        this.add(entryPanel);

        JLabel dictionarySelectLabel = new JLabel("Select a dictionary");
        dictionarySelectLabel.setVisible(true);
        dictionaryPanel.add(dictionarySelectLabel);

        this.dictionaryDropdown.setVisible(true);
        dictionaryPanel.add(this.dictionaryDropdown);

        this.dictionaryDropdownButton.setVisible(true);
        dictionaryPanel.add(this.dictionaryDropdownButton);

        this.dictionaryLabel.setVisible(true);
        dictionaryPanel.add(this.dictionaryLabel);

        this.entryTable.setVisible(true);
        this.entryTable.setEnabled(false);
        tablePanel.add(this.entryTable);

        JLabel entryAddLabel = new JLabel("Add a dictionary entry: ");
        entryAddLabel.setVisible(true);
        entryPanel.add(entryAddLabel);

        this.keyTextField.setToolTipText("Key-Word");
        this.keyTextField.setPreferredSize(new Dimension(100, 20));
        this.keyTextField.setVisible(true);
        entryPanel.add(this.keyTextField);

        this.valueTextField.setToolTipText("Value-Word");
        this.valueTextField.setPreferredSize(new Dimension(100, 20));
        this.valueTextField.setVisible(true);
        entryPanel.add(this.valueTextField);

        this.entryAddButton.setVisible(true);
        entryPanel.add(this.entryAddButton);
    }

    public void refresh(DictionaryController handler) {
        DefaultComboBoxModel<String> dropdownModel = (DefaultComboBoxModel<String>) this.dictionaryDropdown.getModel();
        dropdownModel.removeAllElements();
        handler.getDictionaryNames().forEach(dropdownModel::addElement);
        dropdownModel.setSelectedItem(handler.getCurrentDictionary().getName());

        this.dictionaryLabel.setText("Current dictionary: " + handler.getCurrentDictionary().getName());
        DefaultTableModel tableModel = (DefaultTableModel) this.entryTable.getModel();
        tableModel.getDataVector().removeAllElements();
        handler.getCurrentDictionary().getEntries()
                .forEach(dictionaryEntry -> tableModel.addRow(new Object[]{dictionaryEntry.getKey(), dictionaryEntry.getValue()}));

        tableModel.fireTableDataChanged();
        SwingUtilities.updateComponentTreeUI(this);
    }

    public JButton getDictionaryDropdownButton() {
        return this.dictionaryDropdownButton;
    }

    public JButton getEntryAddButton() {
        return this.entryAddButton;
    }

    public JComboBox<String> getDictionaryDropdown() {
        return dictionaryDropdown;
    }

    public JTextField getKeyTextField() {
        return keyTextField;
    }

    public JTextField getValueTextField() {
        return valueTextField;
    }
}
