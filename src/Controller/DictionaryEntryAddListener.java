package Controller;

import Model.DictionaryEntryModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DictionaryEntryAddListener implements ActionListener {

    private final DictionaryController dictionaryController;
    private final JTextField keyTextField;
    private final JTextField valueTextField;

    public DictionaryEntryAddListener(DictionaryController dictionaryController,
                                      JTextField keyTextField,
                                      JTextField valueTextField) {
        this.dictionaryController = dictionaryController;
        this.keyTextField = keyTextField;
        this.valueTextField = valueTextField;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.keyTextField.getText().equals("") || this.valueTextField.getText().equals("")) return;
        System.out.println(this.keyTextField.getText() + " -> " + this.valueTextField.getText());
        this.dictionaryController.addEntry(new DictionaryEntryModel(this.keyTextField.getText(), this.valueTextField.getText()));
    }

}
