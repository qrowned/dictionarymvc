package Controller;

import Model.DictionaryEntryModel;
import Model.DictionaryModel;
import View.DictionaryView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DictionaryController {

    private final List<DictionaryModel> dictionaries = new ArrayList<>();

    private final DictionaryView view;

    private DictionaryModel currentDictionaryModel;

    public DictionaryController(DictionaryView view) {
        this.view = view;
        this.addActionListener();
    }

    public void addDictionary(DictionaryModel dictionaryModel) {
        if (this.currentDictionaryModel == null) this.currentDictionaryModel = dictionaryModel;
        this.dictionaries.add(dictionaryModel);
        this.view.refresh(this);
    }

    public Optional<DictionaryModel> getDictionary(String name) {
        return this.dictionaries.stream()
                .filter(dictionaryModel -> dictionaryModel.getName().equalsIgnoreCase(name))
                .findFirst();
    }

    public Optional<DictionaryEntryModel> searchEntry(DictionaryModel dictionaryModel, String word) {
        return dictionaryModel.getEntries().stream()
                .filter(entry -> entry.getKey().equalsIgnoreCase(word))
                .findFirst();
    }

    public Optional<DictionaryEntryModel> searchEntry(String word) {
        return this.searchEntry(this.currentDictionaryModel, word);
    }

    public void addEntry(DictionaryModel dictionaryModel, DictionaryEntryModel entry) {
        dictionaryModel.getEntries().add(entry);
        this.view.refresh(this);
    }

    public void addEntry(DictionaryEntryModel entry) {
        this.addEntry(this.currentDictionaryModel, entry);
    }

    public List<String> getDictionaryNames() {
        return this.dictionaries.stream()
                .map(DictionaryModel::getName)
                .collect(Collectors.toList());
    }

    public List<DictionaryModel> getDictionaries() {
        return this.dictionaries;
    }

    public DictionaryModel getCurrentDictionary() {
        return currentDictionaryModel;
    }

    public void setCurrentDictionary(DictionaryModel currentDictionaryModel) {
        this.currentDictionaryModel = currentDictionaryModel;
        this.view.refresh(this);
    }

    private void addActionListener() {
        this.view.getDictionaryDropdownButton().addActionListener(new DictionaryDropdownListener(this, this.view.getDictionaryDropdown()));
        this.view.getEntryAddButton().addActionListener(new DictionaryEntryAddListener(this, this.view.getKeyTextField(), this.view.getValueTextField()));
    }

}
