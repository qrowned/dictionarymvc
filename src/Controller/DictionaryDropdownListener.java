package Controller;

import Model.DictionaryModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DictionaryDropdownListener implements ActionListener {

    private final DictionaryController handler;
    private final JComboBox<String> box;

    public DictionaryDropdownListener(DictionaryController handler,
                                      JComboBox<String> box) {
        this.handler = handler;
        this.box = box;
    }

    public void actionPerformed(ActionEvent ae) {
        String name = (String) this.box.getSelectedItem();
        DictionaryModel dictionaryModel = this.handler.getDictionary(name).orElseThrow(UnsupportedOperationException::new);
        System.out.println(dictionaryModel.getName());
        this.handler.setCurrentDictionary(dictionaryModel);
    }

}
