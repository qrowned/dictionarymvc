import Controller.DictionaryController;
import Model.DictionaryModel;
import View.DictionaryView;

public class DictionaryApplication {

    public static void main(String[] args) {
        DictionaryView dictionaryView = new DictionaryView();
        DictionaryController handler = new DictionaryController(dictionaryView);
        handler.addDictionary(new DictionaryModel("Deutsch - Englisch"));
        handler.addDictionary(new DictionaryModel("Englisch - Deutsch"));

        dictionaryView.refresh(handler);
        dictionaryView.setVisible(true);
    }

}
